import RPi.GPIO as GPIO
import time, signal, sys
from keyboard.gpiokb import GPIOKeyboard

class KB4x4(GPIOKeyboard):
    def __init__(self, p1 = 11, p2 = 12, p3 = 13, p4 = 15,
            p5 = 16, p6 = 18, p7 = 19, p8 = 21):
        GPIO.setmode(GPIO.BOARD)
        self.repeat = True
        self.value = -1
        self.pins = [p1, p2, p3, p4, p5, p6, p7, p8]
        for i in range(8):
            GPIO.setup(self.pins[i], GPIO.IN, pull_up_down = GPIO.PUD_UP)

    def setScanPin(self, pin):
        for i in range(4):
            if i == pin:
                #GPIO.setup(self.pins[i], GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
                GPIO.setup(self.pins[i], GPIO.OUT)
                GPIO.output(self.pins[i], GPIO.LOW)
            else:
                #GPIO.setup(self.pins[i], GPIO.OUT)
                #GPIO.output(self.pins[i], GPIO.HIGH)
                GPIO.setup(self.pins[i], GPIO.IN, pull_up_down = GPIO.PUD_UP)

    def getRawInput(self):
        while 1:
            x = -1
            y = -1
            for i in range(4): # set one pin to low repeatly for the first 4 pins
                self.setScanPin(i)
                time.sleep(0.01)
                for j in range(4):
                    # Get pin status for the last 4 pins
                    # If all the pins are higt, no key is pressed
                    if not GPIO.input(self.pins[j + 4]):
                        x = i
                        y = j
                        break
                if x >= 0 or y >= 0:
                    break
            # Check if key pressed
            if x >= 0 or y >= 0:
                # Key pressed
                value = x * 4 + y
                if value == self.value and self.repeat:
                    continue # repeat key press, skip
                # not a redundand key press, return value and set repeat flag
                self.repeat = True
                self.value = x * 4 + y
                return self.value
            else:
                # Key released, clean repeat flag
                self.repeat = False

    def getKeyInput(self):
        kb_in = self.getRawInput()
        if kb_in ==  0:
            return 'D'
        elif kb_in == 1:
            return 'C'
        elif kb_in == 2:
            return 'B'
        elif kb_in == 3:
            return 'A'
        elif kb_in == 4:
            return '#'
        elif kb_in == 5:
            return '9'
        elif kb_in == 6:
            return '6'
        elif kb_in == 7:
            return '3'
        elif kb_in == 8:
            return '0'
        elif kb_in == 9:
            return '8'
        elif kb_in == 10:
            return '5'
        elif kb_in == 11:
            return '2'
        elif kb_in == 12:
            return '*'
        elif kb_in == 13:
            return '7'
        elif kb_in == 14:
            return '4'
        elif kb_in == 15:
            return '1'

def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    GPIO.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
    kb = KB4x4()
    while 1:
        print kb.getKeyInput()
