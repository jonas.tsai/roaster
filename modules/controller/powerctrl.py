#!/usr/bin/python
import time, math, signal, sys, thread
from threading import Thread
from libs.sysinfo import SystemInfo
if not SystemInfo.getSimulation():
    from controller.rss import RSS
from libs.subject import Subject
from controller.ctrl import *

class PowerController(Subject, KnobBaseCtrl):
    def __init__(self, ctrl_pin = 12, period = 5, support_knob = False):
        super(PowerController, self).__init__(support_knob)
        self.ctrl_pin = ctrl_pin
        self.period = period
        self.rss = RSS(ctrl_pin)
        self.support_knob = support_knob

    def outputWatt(self):
        while self.running:
            rss_enable = self.period * self.power
            rss_disable = self.period * (1 - self.power)

            self.rss.enable()
            time.sleep(rss_enable)
            self.rss.disable()
            time.sleep(rss_disable)

    def setRunning(self, run):
        self.running = run
        if self.running:
            self.tr = Thread(target=self.outputWatt, args=())
            self.tr.start()
        else:
            self.tr.join()

    def getPowerPeriod(self):
        return self.period

    def setPowerPeriod(self, value):
        if value < 2:
            value = 0
        elif value > 60:
            value = 60
        self.period = value

    def cleanup(self):
        '''Selective GPIO cleanup'''
        self.rss.cleanup()

class FakePowerController(Subject, KnobBaseCtrl):
    def __init__(self, ctrl_pin = 0, period = 0, support_knob = False):
        super(FakePowerController, self).__init__()
        KnobBaseCtrl.__init__(self, False)

    def getPowerPeriod(self):
        return 5

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    pc.setRunning(False)
    t1.join()
    t2.join()
    pc.cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":

    # Multi-chip example
    '''
    rss.enable()
    time.sleep(1000)
    '''
    pc = PowerController()
    t1 = Thread(target=pc.checkChanged, args=(2.3,))
    t2 = Thread(target=pc.outputWatt, args=())
    t1.start()
    t2.start()
    #t1 = thread.start_new_thread(pc.checkChanged, (0.3,))
    #t2 = thread.start_new_thread(pc.outputWatt, ())
    while 1 :
        time.sleep(1.2)
        #print "Power:  %.f" % pc.getPower()

