#!/usr/bin/python
from libs.subject import Subject
from libs.sysinfo import SystemInfo

if not SystemInfo.getSimulation():
    import RPi.GPIO as GPIO
    from ads1015.Adafruit_ADS1x15 import ADS1x15

class Controller(object):
    def __init__(self):
        self.power = 0
        self.oldPower = -1
        self.running = True
        self.quickSetting = True

    def checkChanged(self):
        pass

    def getPower(self):
        return self.power

    def setRunning(self, run):
        self.running = run

    def setPower(self, power):
        self.quickSetting = True
        power = float(power)
        if power > 100:
            power = 100
        elif power < 0:
            power = 0
        self.power = power / 100
        self.notify()
        return power

    def getPower(self):
        return round(self.power, 2) * 100

    def cleanup(self):
        pass

class KnobBaseCtrl(Controller):
    def __init__(self, support_knob = False):
        super(KnobBaseCtrl, self).__init__()
        self.support_knob = support_knob
        self.adcPin1 = 0
        self.adcPin2 = 1
        if self.support_knob:
            self.adc = ADS1x15()
            # change more than 5% will force entering auto mode
            # In auto mode, power is controlled according to adc
            self.manual_threshold = 0.05
            self.quickSetting = False
            self.gain = 4096
            self.sps = 250

    def checkChanged(self):
        if not self.support_knob:
            return
        isChanged = False
        volt_full = self.adc.readADCSingleEnded(self.adcPin1, self.gain, self.sps)
        volt_set = self.adc.readADCSingleEnded(self.adcPin2, self.gain, self.sps)
        #print "current:", volt_set, "mV, total:", volt_full, "mV"
        percent = float(volt_set / volt_full)
        if percent > 1:
            percent = 1
        elif percent < 0:
            percent = 0
        #print "Power:  %.f" % (round(percent, 2) * 100)
        if self.oldPower == -1:
            self.oldPower = percent
            isChanged = True
        diff = math.fabs(percent - self.oldPower)
        if self.quickSetting:
            if diff > self.manual_threshold: # entering auto mode
                self.quickSetting = False
                self.oldPower = self.power = percent
                isChanged = True
        else:
            if diff >= 0.01:
                isChanged = True
            self.oldPower = self.power = percent
        # Notify all observer if power setting changed
        if isChanged:
            self.notify()

    def getPower(self):
        return self.power

    def setRunning(self, run):
        self.running = run

    def setPower(self, power):
        self.quickSetting = True
        power = float(power)
        if power > 100:
            power = 100
        elif power < 0:
            power = 0
        self.power = power / 100
        self.notify()
        return power

    def getPower(self):
        return round(self.power, 2) * 100

