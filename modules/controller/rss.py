#!/usr/bin/python
import RPi.GPIO as GPIO
import time

class RSS(object):
    def __init__(self, ctrl_pin, board = GPIO.BOARD):
        self.ctrl_pin = ctrl_pin
        self.board = board

        # Initialize needed GPIO
        GPIO.setmode(self.board)
        GPIO.setup(self.ctrl_pin, GPIO.OUT)

        # Pull chip select high to make chip inactive
        GPIO.output(self.ctrl_pin, GPIO.LOW)

    def enable(self):
        GPIO.output(self.ctrl_pin, GPIO.HIGH)

    def disable(self):
        GPIO.output(self.ctrl_pin, GPIO.LOW)

    def cleanup(self):
        '''Selective GPIO cleanup'''
        GPIO.setup(self.ctrl_pin, GPIO.IN)

class MAX31855Error(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

if __name__ == "__main__":

    # Multi-chip example
    import time
    ctrl_pin = 12
    rss = RSS(ctrl_pin)
    '''
    rss.enable()
    time.sleep(1000)
    '''
    while 1 :
        rss.enable()
        time.sleep(1)
        rss.disable()
        time.sleep(1)
    rss.cleanup()
