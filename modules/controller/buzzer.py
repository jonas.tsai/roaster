#!/usr/bin/python
import time, math, signal, sys, thread
import RPi.GPIO as GPIO
from threading import Thread

class Buzzer():
    def __init__(self, pwm_pin=32):
        self.pwm_pin = pwm_pin
        self.running = True
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pwm_pin, GPIO.OUT)
        self.freq = 0
        self.interval = 0
        self.pwm = GPIO.PWM(self.pwm_pin, 1000)
        self.pwm.start(0)

    def setFrequency(self, freq):
        if freq > 5:
            freq = 5
        elif freq < 0:
            freq = 0
        self.freq= freq
        self.pwm.ChangeFrequency(2000 * freq)
        #self.changed = True

    def setInterval(self, interval):
        if interval > 5:
            interval = 5
        elif interval < 0:
            interval = 0
        self.interval = interval
        #self.changed = True

    def outputSound(self):
        while self.running:
            if self.freq == 0 or self.interval == 0:
                self.pwm.ChangeDutyCycle(0)
                time.sleep(0.1)
                continue
            #if self.changed:
            #    self.changed = False

            enable = self.interval * 0.4
            disable = self.interval * 0.6
            self.pwm.ChangeDutyCycle(50)
            time.sleep(enable)
            self.pwm.ChangeDutyCycle(0)
            time.sleep(disable)

    def setRunning(self, run):
        self.running = run
        if not run:
            self.pwm.stop()

    def setPower(self, power):
        self.quickSetting = True
        power = float(power)
        if power > 100:
            power = 100
        elif power < 0:
            power = 0
        self.power = power / 100

    def cleanup(self):
        '''Selective GPIO cleanup'''
        GPIO.setup(self.pwm_pin, GPIO.IN)

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    buzzer.setRunning(False)
    #t1.join()
    #t2.join()
    buzzer.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":

    # Multi-chip example
    '''
    rss.enable()
    time.sleep(1000)
    '''
    buzzer = Buzzer()
    t1 = Thread(target=buzzer.outputSound, args=())
    #t2 = Thread(target=pc.outputWatt, args=())
    t1.start()
    #t2.start()
    #t1 = thread.start_new_thread(pc.getPowerSetting, (0.3,))
    #t2 = thread.start_new_thread(pc.outputWatt, ())
    freq = 0
    interval = 0
    while 1 :
        print "Current frequency is ", freq
        print "Current interval is ", interval
        freq = input("Enter new frequency: ")
        interval = input("Enter new interval: ")
        buzzer.setFrequency(freq)
        buzzer.setInterval(interval)

