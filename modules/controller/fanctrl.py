#!/usr/bin/python
import time, math, signal, sys, thread
from threading import Thread
from libs.subject import Subject
from controller.ctrl import *

class FanController(Subject, KnobBaseCtrl):
    def __init__(self, pwm_pin = 29, en_pin = 31, support_knob = False):
        super(FanController, self).__init__(support_knob)
        self.support_knob = support_knob
        self.pwm_pin = pwm_pin
        self.en_pin = en_pin
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pwm_pin, GPIO.OUT)
        GPIO.setup(self.en_pin, GPIO.OUT)
        GPIO.output(self.en_pin, GPIO.LOW)
        self.pwm = GPIO.PWM(self.pwm_pin, 1000)
        self.pwm.start(self.power)
        self.adcPin1 = 2
        self.adcPin2 = 3

    def setPower(self, power):
        power = super(FanController, self).setPower(power)
        if power == 0:
            GPIO.output(self.en_pin, GPIO.LOW)
        else:
            GPIO.output(self.en_pin, GPIO.HIGH)
        self.pwm.ChangeDutyCycle(power)

    def cleanup(self):
        '''Selective GPIO cleanup'''
        GPIO.setup(self.pwm_pin, GPIO.IN)
        GPIO.setup(self.en_pin, GPIO.IN)

class FakeFanController(Subject, KnobBaseCtrl):
    def __init__(self, pwm_pin = 0, en_pin = 0, support_knob = False):
        super(FakeFanController, self).__init__()
        KnobBaseCtrl.__init__(self, False)

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    fan.setRunning(False)
    #t1.join()
    #t2.join()
    fan.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":

    fan = FanController()
    #t1 = Thread(target=pc.getPowerSetting, args=(2.3,))
    #t2 = Thread(target=pc.outputWatt, args=())
    #t1.start()
    #t2.start()
    #t1 = thread.start_new_thread(pc.getPowerSetting, (0.3,))
    #t2 = thread.start_new_thread(pc.outputWatt, ())
    duty = 0
    while 1 :
        print "Current duty cycle is ", duty
        duty = input("Enter new duty cycle: ")
        fan.setPower(duty)
        #print "Power:  %.f" % pc.getPower()

