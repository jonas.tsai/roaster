#!/usr/bin/python

class Sensor(object):
    def __init__(self):
        self.isCheck = True
    def checkChanged(self):
        pass
    def setIsChecking(self, isCheck):
        self.isCheck = isCheck
    def cleanup(self):
        pass

class ThermoSensor(Sensor):
    def __init__(self):
        self.old_temp = -1
        self.temp = 125
        super(ThermoSensor, self).__init__()

    def checkChanged(self):
        super(ThermoSensor, self).checkChanged()
        if not self.isCheck:
            return
        temp = int(self.read_tmperature())
        if temp != self.old_temp:
            self.old_temp = temp
            self.notify()
            print "Notify..........."
            return

    def read_tmperature(self):
        return self.temp

    def get_cached_temperature(self):
        return self.old_temp

    # This method should be called in simulation mode
    def set_temperature(self, temp):
        self.temp = temp

class HumiditySensor(Sensor):
    def __init__(self):
        self.old_humidity = -1
        self.humi = 73
        super(HumiditySensor, self).__init__()

    def checkChanged(self):
        super(HumiditySensor, self).checkChanged()
        if not self.isCheck:
            return
        humidity = int(self.read_humidity())
        if humidity != self.old_humidity:
            self.old_humidity = humidity
            self.notify()
            return

    def read_humidity(self):
        return self.humi

    def get_cached_humidity(self):
        return self.old_humidity

    # This method should be called in simulation mode
    def set_humidity(self, humi):
        self.humi = humi

