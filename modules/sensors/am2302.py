#!/usr/bin/python
from libs.subject import Subject
import struct, array, time
import Adafruit_DHT

class AM2302(Subject, ThermoSensor, HumiditySensor):
    def __init__(self, data_pin):
        super(AM2302, self).__init__()
        self.sensor = Adafruit_DHT.AM2302
        self.data_pin = data_pin
        self.isCheck = True
        time.sleep(.1)
        self.old_temp = -1
        self.old_humidity = -1
        self.delayCount = 0

    def forceUpdate(self):
        humidity, temp = Adafruit_DHT.read_retry(self.sensor, self.data_pin)
        if temp != self.old_temp:
            self.old_temp = temp
            self.old_humidity = humidity
            self.notify()
            return
        if humidity != self.old_humidity:
            self.old_humidity = humidity
            self.notify()
            return

    def checkChanged(self):
        if not self.isCheck:
            return
        if self.delayCount == 0:
            self.forceUpdate()
        self.delayCount += 1
        if self.delayCount > 20:
            self.delayCount = 0

    def setIsChecking(self, isCheck):
        self.isCheck = isCheck

    def get_cached_temperature(self):
        return self.old_temp

    def get_cached_humidity(self):
        return self.old_humidity

    def read_tmperature(self):
        humidity, temp = Adafruit_DHT.read_retry(self.sensor, self.data_pin)
        return temp

    def read_humidity(self):
        humidity, temp = Adafruit_DHT.read_retry(self.sensor, self.data_pin)
        return humidity

if __name__ == "__main__":
    obj = AM2302(25)
    print "Temp:", obj.read_tmperature(), "C"
    print "Humid:", obj.read_humidity(), "% rH"

