#!/usr/bin/python
from max31855 import MAX31855, MAX31855Error

cs_pin = 24
clock_pin = 19 #23
data_pin = 26 #22
units = "c"
thermocouple = MAX31855(cs_pin, clock_pin, data_pin, units)
print(thermocouple.get())
thermocouple.cleanup()

