#!/usr/bin/python
from libs.subject import Subject
from sensors.sensor import *

class FakeHTSensor(Subject, ThermoSensor, HumiditySensor):
    def __init__(self):
        super(FakeHTSensor, self).__init__()

    def read_tmperature(self):
        return self.temp

    def read_humidity(self):
        return self.humi

class FakeThermocouple(Subject, ThermoSensor):
    def __init__(self):
        super(FakeThermocouple, self).__init__()
        #ThermoSensor self).__init__()

    def read_tmperature(self):
        return self.temp
