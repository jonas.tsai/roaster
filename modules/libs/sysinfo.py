import socket
import fcntl
import struct

import time
import commands

class SystemInfo():
    #def __init__(self):
    isInit = False
    simulation = False

    def __init__(self):
        self.roaster_temperature = -1
        self.env_temperature = -1
        self.env_humidity = -1
        self.isChanged = True
        self.running = False

    def setRunning(self, run):
        self.running = run

    def setRoasterSensor(self, sensor):
        self.roaster_sensor = sensor

    def setEnvSensor(self, sensor):
        self.env_sensor = sensor

    def setPowerController(self, pc):
        self.pc = pc

    def setFanController(self, fc):
        self.fc = fc

    def setTimer(self, timer):
        self.timer = timer

    def getRoasterTemperature(self):
        return self.roaster_temperature

    def getEnvTemperature(self):
        return self.env_temperature

    def getEnvHumidity(self):
        return self.env_humidity

    def updateSensorInfo(self, sleep = 0.1):
        if sleep == 0:
            sleep = 0.1
        while self.running:
            self.roaster_sensor.checkChanged()
            self.env_sensor.checkChanged()
            self.pc.checkChanged()
            self.fc.checkChanged()
            time.sleep(sleep)

    def updateTimerInfo(self, sleep = 0.1):
        if sleep == 0:
            sleep = 0.1
        size = len(self.timer)
        while self.running:
            for i in range(size):
                self.timer[i].checkChanged()
            time.sleep(sleep)

    def getIPAddr(self, ifname):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            info = fcntl.ioctl(s.fileno(), 0x8915,  # SIOCGIFADDR
                struct.pack('256s', ifname[:15]))
        except IOError as e:
            return 'n/a'

        return socket.inet_ntoa(info[20:24])

    @staticmethod
    def setSimulation(isSimu):
        SystemInfo.simulation = isSimu

    @staticmethod
    def getSimulation():
        if not SystemInfo.isInit:
            SystemInfo.isInit = True
            status, output = commands.getstatusoutput("cat /proc/cpuinfo | grep BCM")
            if len(output) > 0:
                SystemInfo.simulation = False
            else:
                SystemInfo.simulation = True
        return SystemInfo.simulation

if __name__ == "__main__":
    sysinfo = SystemInfo()
    print sysinfo.getIPAddr('eth0')
    print sysinfo.getIPAddr('wlan0')
