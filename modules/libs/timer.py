#!/usr/bin/python
from libs.subject import Subject
import time

class ElapseTimer(Subject):
    START = 0
    STOP = 1
    RESET = 2
    def __init__(self):
        super(ElapseTimer, self).__init__()
        self.status = self.STOP
        self.startTime = 0
        self.stopTime = 0
        self.old_s = -1

    def setTimer(self, cmd):
        # Same command, skip it
        if self.status == cmd:
            return
        if cmd == self.START:
            diff = self.startTime - self.stopTime
            self.startTime = time.time()
            self.stopTime = time.time()
            self.startTime += diff
            self.old_s = -1
        elif cmd == self.STOP and self.status == self.START:
            self.stopTime = time.time()
        elif cmd == self.RESET:
            self.startTime = 0
            self.stopTime = 0
        self.status = cmd
        self.notify()

    def checkChanged(self):
        if self.status == self.START:
            self.stopTime = time.time()
        else:
            return
        current_s = int((self.stopTime - self.startTime) % 60)
        if current_s != self.old_s:
            self.old_s = current_s
            self.notify()

    def getStatus(self):
        return self.status

    def getElapseTime(self):
        return (self.stopTime - self.startTime)

    def getStr(self):
        if self.status == self.START:
            self.stopTime = time.time()
        m, s = divmod(self.stopTime - self.startTime, 60)
        return "%4dm, %2ds" % (m, s)

