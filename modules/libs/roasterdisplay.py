#!/usr/bin/python
from libs.sysinfo import SystemInfo
if SystemInfo.getSimulation():
    from lcd.display import *
else:
    import smbus
    from lcd.lcd_2004 import *
import time, math, signal, sys
#from timer.timer import ElapseTimer

class RoasterDisplay(object):
    def __init__(self):
        if SystemInfo.getSimulation():
            self.lcd = FakeLCDDisplay()
        else:
            self.lcd = LCD()
            self.lcd.begin(LCD_COLUMN_NUM, LCD_ROW_NUM, LCD_2LINE)
            self.lcd.setBacklightPin(BACKLIGHT_PIN, BACKLIGHT_FLAG)
            self.lcd.backlightOn()
            self.lcd.clear()
            self.lcd.home()
        #self.t1 = -999
        #self.t2 = -999
        #self.hum = 0
        #self.heatPower = 0
        #self.power_period = 0
        #self.old_t1 = 0
        #self.old_t2 = 0
        #self.old_hum = 0
        #self.old_heatPower = 0
        #self.old_power_period = 0

        self.disp_forceUpdate = True
        self.disp_index = 0
        self.disp_showWelcome = False

        self.page_changed = True
        #self.page0_changed = True
        #self.page1_changed = True
        self.running = True

    def showWelcome(self):
        self.disp_showWelcome = True
        self.disp_forceUpdate = True

    def setWelcomeMsg(self):
        self.lcd.setCursor(0, 1)
        self.lcd.showStr("Good Day!", 0, ALIGN_CENTER)
        self.lcd.showStr("Raspberry Pi", 1, ALIGN_CENTER)
        self.lcd.showStr("Roaster Controller", 2, ALIGN_CENTER)
        self.lcd.showStr("meetate.blogspot.com", 3, ALIGN_CENTER)

    def backlightOn(self):
        self.lcd.backlightOn()

    def backlightOff(self):
        self.lcd.backlightOff()

    def setCurrentState(self, state):
        self.state = state
        self.cleanScreen()

    def changeDisplayIndex(self):
        self.disp_forceUpdate = True
        if self.disp_showWelcome:
            self.disp_showWelcome = False
            return
        if self.disp_index == 0:
            self.disp_index = 1
        else:
            self.disp_index = 0

    def setRunning(self, run):
        self.running = run

    def isPage0Changed(self):
        if self.page0_changed:
            self.page0_changed = False
            return True
        return False
        '''
        self.t1 = int(self.roaster_sensor.read_tmperature())
        self.heatPower = int(self.pc.getPower())
        if self.old_t1 != self.t1:
            return True
        elif self.old_heatPower != self.heatPower:
            return True
        for i in range(len(self.timer)):
            if self.timer[i].isChanged():
                return True
        return False
        '''

    '''
    def showPage0(self):
        self.lcd.setCursor(0, 1)
        self.lcd.showStr(self.getThermo1Str(), 0)
        self.lcd.showStr(self.getTimerStr(0), 1)
        self.lcd.showStr(self.getTimerStr(1), 2)
        self.lcd.showStr(self.getPowerStr() + " " + self.getFanStr(), 3)
    '''

    def isPage1Changed(self):
        if self.page1_changed:
            self.page1_changed = False
            return True
        return False
        '''
        self.t2 = int(self.env_sensor.read_tmperature())
        self.hum = int(self.env_sensor.read_humidity())
        self.power_period = self.pc.getPowerPeriod()
        if self.old_t2 != self.t2:
            return True
        elif self.old_hum != self.hum:
            return True
        elif self.old_power_period != self.power_period:
            return True
        return False
        '''

    def showPage1(self):
        self.lcd.setCursor(0, 1)
        self.lcd.showStr(self.getIPInfoStr('eth'), 0)
        self.lcd.showStr(self.getIPInfoStr('wlan'), 1)
        #self.lcd.showStr(self.getPowerPeriodStr(), 2)
        self.lcd.showStr(self.getNowTimeStr(), 2)
        temp = self.env_sensor.get_cached_temperature()
        hum = self.env_sensor.get_cached_humidity()
        self.lcd.showStr(self.getEnvTUStr(temp, hum), 3)

    def update(self, subject):
        # TODO, this function might not needed
        if subject == self.state:
            self.page_changed = True
        '''
        elif subject == self.env_sensor:
            self.page1_changed = True
        elif subject == self.pc:
            self.page0_changed = True
        elif subject == self.fc:
            self.page0_changed = True
        elif subject == self.timer[0] or subject == self.timer[1]:
            self.page0_changed = True
        else:
            print "Unknown subject"
        '''

    def showPage2(self):
        log = self.recorder.get_current_log()
        self.showLog(log)

    def showPage3(self):
        log = self.recorder.get_old_log()
        self.showLog(log)

    def showDisplayIndex(self, index):
        self.disp_index = index
        self.disp_forceUpdate = True
        print "Show page " + str(index)
        #if index == 0:
        #    self.showPage0()
        #elif index == 1:
        #    self.showPage1()
        #elif index == 2:
        #    self.showPage2()
        #else:
        #    print "No such page..."

    def updateDisplay(self, sleep):
        forceUpdate = True
        while self.running:
            if self.disp_forceUpdate:
                self.cleanScreen()
                self.disp_forceUpdate = False
                forceUpdate = True
            if self.state.isChanged() or forceUpdate:
                self.lcd.setCursor(0, 1)
                for i in range(0, 4):
                    strToShow = self.state.getContentToShow(i)
                    if len(strToShow) > 20:
                        strToShow = strToShow[0:20]
                    strToShow = strToShow.ljust(20)
                    self.lcd.showStr(strToShow, i)

            '''
            if self.disp_showWelcome:
                if forceUpdate:
                    self.setWelcomeMsg()
            elif (self.disp_index == 0):
                if self.isPage0Changed() or forceUpdate:
                    self.env_sensor.setIsChecking(False)
                    self.showPage0()
            elif (self.disp_index == 1):
                if self.isPage1Changed() or forceUpdate:
                    self.env_sensor.setIsChecking(True)
                    self.showPage1()
            elif (self.disp_index == 2):
                if forceUpdate:
                    self.env_sensor.setIsChecking(False)
                    self.showPage2()
            elif (self.disp_index == 3):
                if forceUpdate:
                    self.env_sensor.setIsChecking(False)
                    self.showPage3()
            '''
            if forceUpdate:
                forceUpdate = False
            time.sleep(sleep)

    def cleanScreen(self):
        self.lcd.clear()


# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    stopFlag = True
    display.backlightOff()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":

    stopFlag = False
    display = RoasterDisplay()
    display.showWelcome()
    display.setTimer(0, ElapseTimer.START)
    time.sleep(3.5)
    display.setTimer(1, ElapseTimer.START)
    display.setThermo1(177)
    display.setThermo2(185)
    time.sleep(3)
    display.cleanScreen()
    display.showPage0()
    while 1:
        if (display.isPage0Changed()):
            print "changed"
            display.showPage0()
        time.sleep(0.1)
        if stopFlag:
            break

