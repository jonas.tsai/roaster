#!/usr/bin/python

import time
from copy import deepcopy

# Trigger. In what condition, the roaster setting should be changed
Trigger_Time           = "time"
Trigger_Thermo         = "thermo"

# Action. What action should be taken ?
Action_SetPower        = "power"
Action_SetFan          = "fan"
Action_SetTimer        = "timer2"
Action_SetSound        = "sound"
Action_Enable          = "enable" # enable disabled command

# Status. Current status.
Status_Waiting         = 1
Status_Enabled         = 2
Status_Disabled        = 3

# Debounce
Max_Debounce           = 3

class RoastCmd(object):
    def __init__(self):
        self.trigger = ""
        self.trigger_value = 0
        self.actionPair = []
        self.debounce_count = Max_Debounce
        self.status = Status_Waiting

class RoastProfile(object):
    def __init__(self):
        self.name = ""
        self.rawContent = []
        self.roastcmd = []
        self.roastcmd_rep = []

    def readProfile(self, filePath):
        lines = [line.rstrip('\n') for line in open(filePath)]
        lines = [x.replace(" ", "") for x in lines]
        lines = [x.lower() for x in lines]
        self.rawContent = lines
        # Do not process first line since it is profile name
        self.name = lines[0]
        lines.pop(0)
        for line in lines:
            print line
            cmd = line.split(',')
            print cmd
            if len(cmd) < 4 or len(cmd) %2 != 0:
                print "Error command setting: ", line
                continue

            rCmd = RoastCmd()
            rCmd.trigger = cmd[0]
            rCmd.trigger_value = int(cmd[1])
            if rCmd.trigger == "time":
                rCmd.status = Status_Enabled
            else:
                # For the thermo trigger, the bt should be lower than the trigger value to enable it
                rCmd.status = Status_Waiting

            for i in range(2, len(cmd), 2):
                action = []
                action.append(cmd[i])
                action.append(cmd[i+1])
                rCmd.actionPair.append(action)
            self.roastcmd_rep.append(rCmd)
            self.roastcmd = deepcopy(self.roastcmd_rep)

    def restart(self):
        self.roastcmd = deepcopy(self.roastcmd_rep)

    def saveProfile(self, filePath):
        pass

    def updateCmd(self, timeInS, bt):
        pass

    def getCmdToExecute(self, timeInS, bt):
        actions = []
        for rCmd in self.roastcmd:
            if rCmd.status == Status_Enabled:
                if rCmd.trigger == Trigger_Time:
                    if timeInS >= rCmd.trigger_value:
                        print "Execute time trigger:", rCmd.trigger, rCmd.trigger_value
                        actions.extend(rCmd.actionPair)
                        rCmd.status = Status_Disabled
                elif rCmd.trigger == Trigger_Thermo:
                    if bt >= rCmd.trigger_value:
                        rCmd.debounce_count = rCmd.debounce_count - 1
                        if rCmd.debounce_count <= 0:
                            print "Execute bt trigger:", rCmd.trigger, rCmd.trigger_value
                            actions.extend(rCmd.actionPair)
                            rCmd.status = Status_Disabled
                    else:
                        rCmd.debounce_count = Max_Debounce
                else: # Should be count down
                    rCmd.trigger_value = rCmd.trigger_value - 1
                    print "Decrease %s counter to: %d" % (rCmd.trigger, rCmd.trigger_value)
                    if rCmd.trigger_value <= 0:
                        actions.extend(rCmd.actionPair)
                        rCmd.status = Status_Disabled
            elif rCmd.status == Status_Waiting and rCmd.trigger == Trigger_Thermo:
                # For the thermo trigger, the bt should be lower than the trigger value to enable it
                if bt < rCmd.trigger_value:
                    rCmd.debounce_count = rCmd.debounce_count - 1
                    if rCmd.debounce_count <= 0:
                        print "Enable bt trigger:", rCmd.trigger, rCmd.trigger_value
                        rCmd.status = Status_Enabled
                        rCmd.debounce_count = Max_Debounce
                else:
                    rCmd.debounce_count = Max_Debounce
        # Check action pairs for enable command
        for action in actions:
            #print action[0], action[1]
            if action[0] == Action_Enable:
                for rCmd in self.roastcmd:
                    if rCmd.trigger == action[1]:
                        rCmd.status = Status_Enabled
                        print "Enable %s, Waiting:%d" % (rCmd.trigger, rCmd.trigger_value)
                        break
                actions.remove(action)
        self.updateCmd(timeInS, bt)
        return actions

    def getCommands(self):
        pass

    def printCmd(self):
        print "Profile name:", self.name
        for rCmd in self.roastcmd:
            print "========================="
            print "Trigger:", rCmd.trigger
            print "Trigger value:", rCmd.trigger_value
            for action in rCmd.actionPair:
                print "Set %s to %s" % (action[0], action[1])
            print ""

if __name__ == "__main__":
    a = RoastProfile()
    a.readProfile('../../profile/sample.profile')
    a.printCmd()
    while True:
        ts = input('Time in second: ')
        bt = input('Bean temperature: ')
        actions = a.getCmdToExecute(int(ts), int(bt))
        for action in actions:
            print "Set %s to %s" % (action[0], action[1])
        time.sleep(1)
