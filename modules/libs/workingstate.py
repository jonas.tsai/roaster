from libs.sysinfo import SystemInfo
from datetime import datetime
from libs.timer import ElapseTimer

class WorkingState(object):
    # define mode for input key
    NumKeyMode_Unset = 0
    NumKeyMode_Power = 1
    NumKeyMode_Fan = 2
    NumKeyMode_Timer = 3
    NumKeyMode_SimuThermo = 4
    NumKeyMode_Selection = 5

    def __init__(self):
        self.changed = True
        self.numKeyMode = WorkingState.NumKeyMode_Unset
        self.inputNumber = []
        self.showIndex = 0

    def perform(self, kb):
        print "in Working State"
        pass

    def isChanged(self):
        changed = self.changed
        if self.changed:
            self.changed = False
        return changed

    def setChanged(self, changed):
        self.changed = changed

    def getContentToShow(self, line):
        return "No implementation " + str(line)

    def update(self, subject):
        pass

    def performNumberKey(self, keyIn):
        if self.numKeyMode == self.NumKeyMode_Power:
            self.pc.setPower(keyIn * 10)
            self.numKeyMode = self.NumKeyMode_Unset
        elif self.numKeyMode == self.NumKeyMode_Fan:
            self.fc.setPower(keyIn * 10)
            self.numKeyMode = self.NumKeyMode_Unset
        elif self.numKeyMode == self.NumKeyMode_SimuThermo:
            if keyIn != -1:
                self.inputNumber.append(keyIn)

            if keyIn == -1 or len(self.inputNumber) >= 3:
                inputNum = 0
                numbers = range(0, len(self.inputNumber))
                for index in numbers:
                    inputNum = inputNum * 10
                    inputNum = inputNum + self.inputNumber[index]
                print "Set temperature to", inputNum
                self.tc.set_temperature(inputNum)
                del self.inputNumber[:]
                self.numKeyMode = self.NumKeyMode_Unset


class MainState(WorkingState):
    INIT        = 0
    PRE_HEAT    = 1
    START       = 2
    PRE_STOP    = 3
    STOP        = 4
    def __init__(self):
        super(MainState, self).__init__()
        self.roast_status = MainState.INIT

    def setRoasterSensor(self, sensor):
        self.tc = sensor

    def setPowerController(self, pc):
        self.pc = pc

    def setFanController(self, fc):
        self.fc = fc

    def setTimer(self, timer):
        self.timer = timer

    def setRobot(self, robot):
        self.robot = robot

    def getTimerStr(self, index):
        return "Timer%d:" % (index + 1) + self.timer[index].getStr()

    def setRoastRecorder(self, recorder):
        self.recorder = recorder

    def getPowerStr(self):
        #self.old_heatPower = self.heatPower
        return "Power:%3d" % int(self.pc.getPower())

    def getFanStr(self):
        return "Fan:%3d" % int(self.fc.getPower())

    def getThermo1Str(self):
        temp = self.tc.get_cached_temperature()
        t1Str = "BT:%4d" % temp
        slopeStr = ""
        # Use past 30 seconds to get slope in one minute
        bt_slope = self.recorder.get_slope(30)
        if bt_slope != None:
            slopeStr = " (%d)   " % (bt_slope * 2) # extra space char to clean ')' if bt_slope length decreased
        if SystemInfo.getSimulation():
            return t1Str + "^C" + slopeStr
        else:
            unit = bytearray("xC")
            unit[0] = 223 # 223 shows temperature degree symbol in the LCD
            return t1Str + unit + slopeStr

    def roastStart(self):
        if self.roast_status == MainState.START:
            return
        self.displayIndex = 0
        self.roast_status = MainState.START
        self.recorder.startNewRoastRecord()
        self.timer[0].setTimer(ElapseTimer.RESET)
        self.timer[1].setTimer(ElapseTimer.RESET)
        self.timer[0].setTimer(ElapseTimer.START)
        self.pc.setPower(100)
        self.robot.setRunning(True)
        self.recorder.startRecord()

    def roastStop(self):
        if self.roast_status == MainState.START:
            self.roast_status = MainState.PRE_STOP
            return
        elif self.roast_status == MainState.PRE_STOP:
            self.roast_status = MainState.STOP
        else:
            print "You cannot stop roast in current status..."
            return
        self.timer[0].setTimer(ElapseTimer.STOP)
        self.timer[1].setTimer(ElapseTimer.STOP)
        self.recorder.finishRoast()
        self.pc.setPower(0)
        self.fc.setPower(0)

    def timer2_On_Off(self):
        if self.timer[1].getStatus() == ElapseTimer.START:
            self.timer[1].setTimer(ElapseTimer.STOP)
        else:
            self.timer[1].setTimer(ElapseTimer.START)

    def timer2_Reset(self):
        #if self.timer[1].getStatus() == ElapseTimer.START:
        self.timer[1].setTimer(ElapseTimer.STOP)
        #else:
        self.timer[1].setTimer(ElapseTimer.RESET)

    def update(self, subject):
        if subject == self.tc or subject == self.pc or subject == self.fc or \
            subject == self.timer[0] or subject == self.timer[1]:
            self.changed = True

    def getContentToShow(self, line):
        if line == 0:
            return self.getThermo1Str()
        elif line == 1:
            return self.getTimerStr(0)
        elif line == 2:
            return self.getTimerStr(1)
        elif line == 3:
            return self.getPowerStr() + " " + self.getFanStr()

    def perform(self, kb):
        print "in Main State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == "*":
                print "start coffee roast"
                self.roastStart()
            elif keyCode == "#":
                print "stop coffee roast"
                self.roastStop()
            elif keyCode == "A":
                print "Set Power..."
                if self.numKeyMode != WorkingState.NumKeyMode_Power:
                    self.numKeyMode = WorkingState.NumKeyMode_Power
                else:
                    self.pc.setPower(100)
                    self.numKeyMode = WorkingState.NumKeyMode_Unset
            elif keyCode == "B":
                print "Set Fan..."
                if self.numKeyMode != WorkingState.NumKeyMode_Fan:
                    self.numKeyMode = WorkingState.NumKeyMode_Fan
                else:
                    self.fc.setPower(100)
                    self.numKeyMode = WorkingState.NumKeyMode_Unset
            elif keyCode == "C":
                if self.numKeyMode != WorkingState.NumKeyMode_Timer:
                    self.numKeyMode = WorkingState.NumKeyMode_Timer
                self.timer2_On_Off()
            elif keyCode == 'D':
                return RoastOperate.Menu
            elif keyCode == "T":
                print "Simulate thermocouple"
                if self.numKeyMode != self.NumKeyMode_SimuThermo:
                    self.numKeyMode = self.NumKeyMode_SimuThermo
                else:
                    self.performNumberKey(-1) # Use -1 to indicate input finishing
            elif keyCode == "0":
                if self.numKeyMode == self.NumKeyMode_Timer:
                    self.timer2_Reset()
                else:
                    self.performNumberKey(0)
            elif keyCode.isdigit():
                self.performNumberKey(int(keyCode))
            else:
                print "No such command, key code is", keyCode

class MenuState(WorkingState):
    def getContentToShow(self, line):
        if line == 0:
            return "1. Select Profile"
        elif line == 1:
            return "2. Show System Info"
        elif line == 2:
            return "3. Show Log Info"
        elif line == 3:
            return "C. Return to Main"

    def perform(self, kb):
        print "in Menu State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                return RoastOperate.Main
            elif keyCode == '1':
                return RoastOperate.ProfileList
            elif keyCode == '2':
                return RoastOperate.SysInfo
            elif keyCode == '3':
                return RoastOperate.LogList
            else:
                print "Keycode is", keyCode

class SysInfoState(WorkingState):
    def __init__(self):
        super(SysInfoState, self).__init__()
        self.sysInfoLen = 6

    def setSystemInfo(self, sysinfo):
        self.sysinfo = sysinfo

    def setEnvSensor(self, sensor):
        self.env_sensor = sensor

    def setRobot(self, robot):
        self.robot = robot

    def getEnvTUStr(self, temp, hum):
        #temp = self.env_sensor.get_cached_temperature()
        unit = bytearray("xC")
        unit[0] = 223 # 223 shows temperature symbol in the LCD
        t2Str = "Env:%4d" % temp + unit

        #hum = self.env_sensor.get_cached_humidity()
        humStr = ", %3d%%rh" % hum
        return t2Str + humStr

    def getIPInfoStr(self, ifname):
        if ifname == "eth":
            return "ETH0:%s" % (self.sysinfo.getIPAddr('eth0'))
        elif ifname == "wlan":
            return "WLAN:%s" % (self.sysinfo.getIPAddr('wlan0'))

    def getNowTimeStr(self):
        return "Now: " + datetime.now().strftime('%Y%m%d %H%M%S')

    def getPowerPeriodStr(self):
        #self.old_power_period = self.power_period
        return "Power period:%3d" % self.sysinfo.pc.getPowerPeriod()

    def getSysInfo(self, index):
        if index == 0:
            return self.robot.getWorkingProfile().name
        elif index == 1:
            return self.getIPInfoStr('eth')
        elif index == 2:
            return self.getIPInfoStr('wlan')
        elif index == 3:
            return self.getNowTimeStr()
        elif index == 4:
            temp = self.env_sensor.get_cached_temperature()
            hum = self.env_sensor.get_cached_humidity()
            return self.getEnvTUStr(temp, hum)
        elif index == 5:
            return self.getPowerPeriodStr()

    def getContentToShow(self, line):
        #TODO, logic to get content amount classes can be refactor to one function
        showIndex = self.showIndex + line
        if showIndex >= self.sysInfoLen or showIndex < 0:
            return ""
        return self.getSysInfo(showIndex)

    def perform(self, kb):
        print "in SysInfo State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                return RoastOperate.Menu
            elif keyCode == 'A':
                print "Key up"
                self.showIndex = self.showIndex - 1
                if self.showIndex < 0:
                    self.showIndex = 0
                self.changed = True
            elif keyCode == 'B':
                print "Key down"
                self.showIndex = self.showIndex + 1
                if self.showIndex >= self.sysInfoLen:
                    self.showIndex = self.sysInfoLen - 1
                self.changed = True
            else:
                print "Keycode is", keyCode

class ProfileListState(WorkingState):
    def __init__(self):
        super(ProfileListState, self).__init__()

    def setRobot(self, robot):
        self.robot = robot
        self.profiles = robot.getProfiles()

    def getContentToShow(self, line):
        showIndex = self.showIndex + line
        if showIndex >= len(self.profiles) or showIndex < 0:
            return ""
        return "%d:%s" %(line, self.profiles[showIndex].name)

    def selectProfile(self, index):
        selectIndex = self.showIndex + index
        if selectIndex >= len(self.profiles) or selectIndex < 0:
            return None
        return self.profiles[selectIndex]

    def perform(self, kb):
        print "in Profile List State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                self.showIndex = 0
                return RoastOperate.Menu
            elif keyCode == 'A':
                print "Key up"
                self.showIndex = self.showIndex - 1
                if self.showIndex < 0:
                    self.showIndex = 0
                self.changed = True
            elif keyCode == 'B':
                print "Key down"
                self.showIndex = self.showIndex + 1
                if self.showIndex >= len(self.profiles):
                    self.showIndex = len(self.profiles) - 1
                self.changed = True
            elif keyCode.isdigit():
                profile = self.selectProfile(int(keyCode))
                if profile != None:
                    RoastOperate.ProfileDetail.setProfile(profile)
                    return RoastOperate.ProfileDetail
                else:
                    print "No profile at index", keyCode
            else:
                print "Keycode is", keyCode

class ProfileDetailState(WorkingState):
    def __init__(self):
        super(ProfileDetailState, self).__init__()

    def setRobot(self, robot):
        self.robot = robot

    def setProfile(self, profile):
        self.profile = profile

    def getContentToShow(self, line):
        #TODO, logic to get content amount classes can be refactor to one function
        showIndex = self.showIndex + line
        if showIndex >= len(self.profile.rawContent) or showIndex < 0:
            return ""
        return self.profile.rawContent[showIndex]

    def perform(self, kb):
        print "in Profile Detail State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                return RoastOperate.ProfileList
            elif keyCode == 'A':
                print "Key up"
                #TODO, key up and down amount classes can be refactor to one function
                self.showIndex = self.showIndex - 1
                if self.showIndex < 0:
                    self.showIndex = 0
                self.changed = True
            elif keyCode == 'B':
                print "Key down"
                self.showIndex = self.showIndex + 1
                if self.showIndex >= len(self.profile.rawContent):
                    self.showIndex = len(self.profile.rawContent) - 1
                self.changed = True
            elif keyCode == 'D':
                print "Select this profile"
                self.robot.setWorkingProfile(self.profile)
                return RoastOperate.Main
            else:
                print "Keycode is", keyCode

class LogListState(WorkingState):
    def setRoastRecorder(self, recorder):
        self.recorder = recorder
        self.logs = recorder.getLogs()
        print "Len in state", len(self.logs)

    def getContentToShow(self, line):
        showIndex = self.showIndex + line
        print "Len in state show", len(self.logs)
        if showIndex >= len(self.logs) or showIndex < 0:
            return ""
        return "%d:%s" %(line, self.logs[showIndex].get_start_time().strftime('%Y%m%d_%H%M'))

    def selectLog(self, index):
        selectIndex = self.showIndex + index
        if selectIndex >= len(self.logs) or selectIndex < 0:
            return None
        return self.logs[selectIndex]

    def perform(self, kb):
        print "in Log List State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                return RoastOperate.Menu
            elif keyCode == 'A':
                print "Key up"
            elif keyCode == 'B':
                print "Key down"
            elif keyCode.isdigit():
                log = self.selectLog(int(keyCode))
                if log != None:
                    RoastOperate.LogDetail.setLog(log)
                    return RoastOperate.LogDetail
                else:
                    print "No log at index", keyCode
            else:
                print "Keycode is", keyCode

class LogDetailState(WorkingState):
    def setLog(self, log):
        self.log = log

    def getContentToShow(self, line):
        #TODO, logic to get content amount classes can be refactor to one function
        showIndex = self.showIndex + line
        if showIndex >= len(self.profile.rawContent) or showIndex < 0:
            return ""
        return self.profile.rawContent[showIndex]

    def showLog(self, log):
        self.lcd.setCursor(0, 1)
        # Preparing line 1, roast start time
        if log == None:
            self.lcd.showStr("No log is available", 0)
            return
        startTime = "Start: " + log.get_start_time().strftime('%y%m%d_%H%M')
        self.lcd.showStr(startTime, 0)

        # Preparing line 2, environment temperature and humidity
        temp = log.get_env_temp()
        hum = log.get_env_humi()
        self.lcd.showStr(self.getEnvTUStr(temp, hum), 1)

        # Praprint line 3, bean temperature while starting and stoping roast
        record = log.get_record()
        unit = bytearray("xC")
        unit[0] = 223 # 223 shows temperature symbol in the LCD
        if len (record) > 0:
            if log.get_end_time() == None:
                tempStr = "ST:%4d%s, SP: none " %(record[0].get_bt(), unit)
            else:
                tempStr = "ST:%4d%s, SP:%4d%s" %(record[0].get_bt(), unit, record[-1].get_bt(), unit)
        else:
            tempStr = "No BT is available"
        self.lcd.showStr(tempStr, 2)

        # Preparing line 3, total roast time
        startTime = log.get_start_time()
        stopTime = log.get_end_time()
        if startTime == None or stopTime == None:
            self.lcd.showStr("Roast un-finished", 3)
        else:
            delta = stopTime - startTime
            m, s = divmod(delta.seconds, 60)
            self.lcd.showStr("Total: %4dm, %2ds" % (m, s), 3)
        #self.lcd.showStr(self.getPowerPeriodStr(), 2)
        #self.lcd.showStr(self.getEnvTUStr(), 3)

    def perform(self, kb):
        print "in Log Detail State"
        while True:
            keyCode = kb.getKeyInput()
            if keyCode == 'C':
                return RoastOperate.LogList
            elif keyCode == 'A':
                print "Key up"
            elif keyCode == 'B':
                print "Key down"
            else:
                print "Keycode is", keyCode

class StateMachine:
    def __init__(self, initialState):
        self.currentState = initialState
        self.currentState.run()
        # Template method:
    def runAll(self, inputs):
        for i in inputs:
            print(i)
            self.currentState = self.currentState.next(i)
            self.currentState.run()

class RoastOperate(StateMachine):
    def __init__(self):
        # Initial state
        StateMachine.__init__(self, RoastOperate.Main)

# Static variable initialization:
RoastOperate.Main = MainState()
RoastOperate.Menu = MenuState()
RoastOperate.SysInfo = SysInfoState()
RoastOperate.ProfileList = ProfileListState()
RoastOperate.ProfileDetail = ProfileDetailState()
RoastOperate.LogList = LogListState()
RoastOperate.LogDetail = LogDetailState()

