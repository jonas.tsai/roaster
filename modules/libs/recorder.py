import socket
import fcntl
import struct
import time
import random
import string

from datetime import datetime
from time import strftime
from libs.roastrecord import *
from libs.sysinfo import SystemInfo

class RoastLog(object):
    def __init__(self, second, thermo, power):
        self.second = second
        self.thermo = thermo
        self.power = power

class RoasterRecorder(object):
    def __init__(self):
        self.running = False
        self.name = "coffee name"
        self.desc = "Please describe the coffee"
        self.self_cupping = "How the coffee is tasted"
        self.fire_control = "Descriptions about how the fire and fan are used to roast the coffee beans"
        self.log = None
        self.logs = []

    def setTimer(self, timer):
        self.timer = timer

    def setName(self, name):
        self.name = name

    def setDesc(self, desc):
        self.desc = desc

    def setRoasterSensor(self, sensor):
        self.roaster_sensor = sensor

    def setEnvSensor(self, sensor):
        self.env_sensor = sensor

    def setPowerController(self, pc):
        self.pc = pc

    def setFanController(self, fc):
        self.fc = fc

    def startRecord(self):
        self.running = True

    def startNewRoastRecord(self):
        self.log = roasterlog()
        self.startTime = time.time()
        self.log.set_start_time(datetime.now())
        self.log.set_env_temp(self.env_sensor.get_cached_temperature())
        self.log.set_env_humi(self.env_sensor.get_cached_humidity())
        self.logs.insert(0, self.log)

    def finishRoast(self):
        self.running = False
        if SystemInfo.getSimulation():
            logPath = "/home/jonas/roastlog/" + self.log.get_start_time().strftime('%Y%m%d_%H%M%S.xml')
        else:
            logPath = "/home/pi/roastlog/" + self.log.get_start_time().strftime('%Y%m%d_%H%M%S.xml')
        outfile = open(logPath, "w")
        self.log.set_coffee_name(self.name)
        self.log.set_description(self.desc)
        self.log.set_self_cupping(self.self_cupping)
        self.log.set_fire_ctrl(self.fire_control)
        self.log.set_end_time(datetime.now())
        self.log.set_duration(self.timer.getElapseTime())
        # Create a 6 character random string as a key for this log
        rstr = string.lowercase+string.digits
        self.log.set_random_str(''.join(random.sample(rstr,6)))

        self.log.export(outfile, 0)

    def getDiffWithStartTime(self):
        if not self.running:
            return 0
        diff = time.time() - self.startTime
        if diff < 0:
            diff = 0
        return diff

    def first_crush_start(self):
        self.log.set_first_crack_start(self.getDiffWithStartTime())

    def first_crush_max(self):
        self.log.set_first_crack_max(self.getDiffWithStartTime())

    def first_crush_end(self):
        self.log.set_first_crack_end(self.getDiffWithStartTime())

    def second_crush_start(self):
        self.log.set_second_crack_start(self.getDiffWithStartTime())

    def second_crush_max(self):
        self.log.set_second_crack_max(self.getDiffWithStartTime())

    def second_crush_end(self):
        self.log.set_second_crack_end(self.getDiffWithStartTime())

    def get_current_log(self):
        return self.log

    def getLogs(self):
        print "Len in recorder", len(self.logs)
        return self.logs

    def get_slope(self, past_second = 30):
        if self.log == None:
            return None
        logItem = self.log.get_record()
        if len(logItem) < past_second:
            return None
        else:
            nowT = logItem[-1].get_bt()
            pastT = logItem[len(logItem) - past_second].get_bt()
            return (nowT - pastT)

    def update(self, subject):
        if not self.running:
            return 0
        second = self.timer.getElapseTime()
        bt = self.roaster_sensor.get_cached_temperature()
        power = self.pc.getPower()
        fan = self.fc.getPower()
        print "To be recorded: ", second, bt, power, fan
        self.log.add_record(logitem(second, bt, power, fan))

