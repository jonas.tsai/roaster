#!/usr/bin/python
from libs.profile import *
from libs.timer import *
import os

class RoasterRobot(object):
    def __init__(self):
        self.profile_list_name = "profile/list.cfg"
        self.running = False
        self.profiles = []
        files = [line.rstrip('\n') for line in open(self.profile_list_name)]
        for filename in files:
            print filename
            profile = RoastProfile()
            profile.readProfile("%s/%s" % ("profile", filename))
            profile.printCmd()
            self.profiles.append(profile)
        if len(self.profiles) > 0:
            self.profile = self.profiles[0]

    def setRoasterSensor(self, sensor):
        self.roaster_sensor = sensor

    def setEnvSensor(self, sensor):
        self.env_sensor = sensor

    def setPowerController(self, pc):
        self.pc = pc

    def setFanController(self, fc):
        self.fc = fc

    def setTimer(self, timer):
        self.timer = timer

    def setRunning(self, run):
        if run == True:
            self.profile.restart()
        self.running = run

    def getProfiles(self):
        return self.profiles

    def setWorkingProfile(self, profile):
        self.profile = profile
        #TODO, move this profile to index 0

    def getWorkingProfile(self):
        return self.profile

    # Save profiles into list with new order
    # (Save latest one to line 1 as default profile)
    def saveProfileList():
        # TODO, write to file
        pass

    def update(self, subject):
        if subject != self.timer[0] or not self.running:
            return
        print "========= updated =============="
        # Once the total time update, check time and thermo
        duration = self.timer[0].getElapseTime()
        bt = self.roaster_sensor.get_cached_temperature()
        actions = self.profile.getCmdToExecute(duration, bt)
        for action in actions:
            print "Set %s to %s" % (action[0], action[1])
            if action[0] == Action_SetPower:
                print "Set power to %d" % (int(action[1]))
                self.pc.setPower(int(action[1]))
            elif action[0] == Action_SetFan:
                print "Set fan to %d" % (int(action[1]))
                self.fc.setPower(int(action[1]))
            elif action[0] == Action_SetTimer:
                print "Set timer to %s" %(action[1])
                if action[1] == "start":
                    self.timer[1].setTimer(ElapseTimer.START)
                elif action[1] == "stop":
                    self.timer[1].setTimer(ElapseTimer.STOP)
                elif action[1] == "reset":
                    self.timer[1].setTimer(ElapseTimer.RESET)
                else:
                    print "No such command for timer"
            elif action[0] == Action_SetSound:
                pass


