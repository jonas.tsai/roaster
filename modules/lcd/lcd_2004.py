#!/usr/bin/python
import smbus
from libs.Adafruit_I2C import Adafruit_I2C
from lcd.display import *
import time, math, signal

LCD_CLEARDISPLAY        = 0x01
LCD_RETURNHOME          = 0x02
LCD_ENTRYMODESET        = 0x04
LCD_DISPLAYCONTROL      = 0x08
LCD_CURSORSHIFT         = 0x10
LCD_FUNCTIONSET         = 0x20
LCD_SETCGRAMADDR        = 0x40
LCD_SETDDRAMADDR        = 0x80

# flags for display entry mode
# ---------------------------------------------------------------------------
LCD_ENTRYRIGHT           = 0x00
LCD_ENTRYLEFT            = 0x02
LCD_ENTRYSHIFTINCREMENT  = 0x01
LCD_ENTRYSHIFTDECREMENT  = 0x00

# flags for display on/off and cursor control
# ---------------------------------------------------------------------------
LCD_DISPLAYON            = 0x04
LCD_DISPLAYOFF           = 0x00
LCD_CURSORON             = 0x02
LCD_CURSOROFF            = 0x00
LCD_BLINKON              = 0x01
LCD_BLINKOFF             = 0x00

# flags for display/cursor shift
# ---------------------------------------------------------------------------
LCD_DISPLAYMOVE          = 0x08
LCD_CURSORMOVE           = 0x00
LCD_MOVERIGHT            = 0x04
LCD_MOVELEFT             = 0x00

# flags for function set
# ---------------------------------------------------------------------------
LCD_8BITMODE            = 0x10
LCD_4BITMODE            = 0x00
LCD_2LINE               = 0x08
LCD_1LINE               = 0x00
LCD_5x10DOTS            = 0x04
LCD_5x8DOTS             = 0x00


# Define COMMAND and DATA LCD Rs (used by send method).
# ---------------------------------------------------------------------------
COMMAND                 = 0
DATA                    = 1
FOUR_BITS               = 2

LCD_NOBACKLIGHT         = 0x00
LCD_BACKLIGHT           = 0xFF
BACKLIGHT_OFF           = 0
BACKLIGHT_ON            = 255

LED_OFF                 = 0
LED_ON                  = 1

POSITIVE                = 0
NEGATIVE                = 1

# Define LCM specific Info
I2C_ADDR          = 0x20  #Define I2C Address
BACKLIGHT_PIN     = 3
En_pin            = 2
Rw_pin            = 1
Rs_pin            = 0
D4_pin            = 4
D5_pin            = 5
D6_pin            = 6
D7_pin            = 7
BACKLIGHT_FLAG    = POSITIVE
LCD_COLUMN_NUM    = 20
LCD_ROW_NUM       = 4

class LCD(LCDDisplay):

    def __init__(self, lcd_Addr = 0x20, En = En_pin, Rw = Rw_pin, Rs = Rs_pin,
        d4 = D4_pin, d5 = D5_pin, d6 = D6_pin, d7 = D7_pin):
        self.config(lcd_Addr, En, Rw, Rs, d4, d5, d6, d7)

    def begin(self, cols, lines, dotsize):
        self.init()
        if lines > 1:
            self._displayfunction |= LCD_2LINE

        self._numlines = lines;
        self._cols = cols;

        # for some 1 line displays you can select a 10 pixel high font
        #  ------------------------------------------------------------
        if (dotsize != LCD_5x8DOTS) and (lines == 1):
            self._displayfunction |= LCD_5x10DOTS

        # SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
        # according to datasheet, we need at least 40ms after power rises above 2.7V
        # before sending commands. Arduino can turn on way before 4.5V so we'll wait 50
        #  ---------------------------------------------------------------------------
        time.sleep(0.1)

        # put the LCD into 4 bit or 8 bit mode
        # -------------------------------------
        if not (self._displayfunction & LCD_8BITMODE):
            # this is according to the hitachi HD44780 datasheet
            # figure 24, pg 46
            # we start in 8bit mode, try to set 4 bit mode
            self.send(0x03, FOUR_BITS)
            # delayMicroseconds(4500); // wait min 4.1ms
            time.sleep(1)

            # second try
            self.send(0x03, FOUR_BITS)
            # delayMicroseconds(4500); // wait min 4.1ms
            time.sleep(1)

            # third go!
            self.send(0x03, FOUR_BITS)
            # delayMicroseconds(150);
            time.sleep(0.15)

            # finally, set to 4-bit interface
            self.send(0x02, FOUR_BITS)
        else:
            # this is according to the hitachi HD44780 datasheet
            # page 45 figure 23
            # Send function set command sequence
            self.command(LCD_FUNCTIONSET | self._displayfunction);
            # delayMicroseconds(4500);  // wait more than 4.1ms
            time.sleep(1)

            # second try
            self.command(LCD_FUNCTIONSET | self._displayfunction);
            #delayMicroseconds(150);
            time.sleep(0.15)

            # third go
            self.command(LCD_FUNCTIONSET | self._displayfunction);

        # finally, set # lines, font size, etc.
        self.command(LCD_FUNCTIONSET | self._displayfunction)

        # turn the display on with no cursor or blinking default
        self._displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF
        self.display()

        # clear the LCD
        self.clear()

        # Initialize to default text direction (for romance languages)
        self._displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT
        # set the entry mode
        self.command(LCD_ENTRYMODESET | self._displaymode)

        self.backlightOn()

    def init(self):
        self._displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS
        self.i2c.write(0) #TODO

    def config(self, lcd_Addr, En, Rw, Rs, d4, d5, d6, d7):
        self.i2c = Adafruit_I2C(lcd_Addr)
        self._data_pins = []
        for i in range(4):
            self._data_pins.append(0)

        self._Addr = lcd_Addr
        self._backlightPinMask = 0;
        self._backlightStsMask = LCD_NOBACKLIGHT;
        self._polarity = POSITIVE;

        self._En = ( 1 << En );
        self._Rw = ( 1 << Rw );
        self._Rs = ( 1 << Rs );

        # Initialise pin mapping
        self._data_pins[0] = ( 1 << d4 );
        self._data_pins[1] = ( 1 << d5 );
        self._data_pins[2] = ( 1 << d6 );
        self._data_pins[3] = ( 1 << d7 );

    def display(self):
        self._displaycontrol &= LCD_DISPLAYON
        self.command(LCD_DISPLAYCONTROL | self._displaycontrol)

    def noDisplay(self):
        self._displaycontrol &= ~LCD_DISPLAYON
        self.command(LCD_DISPLAYCONTROL | self._displaycontrol)

    def backlightOn(self):
        self.setBacklight(255)

    def backlightOff(self):
        self.setBacklight(0)

    def clear(self):
        self.command(LCD_CLEARDISPLAY)
        time.sleep(0.5)

    def home(self):
        self.command(LCD_RETURNHOME)
        time.sleep(0.5)

    def setCursor(self, col, row):
        row_offsetsDef = [0x00, 0x40, 0x14, 0x54]
        row_offsetsLarge = [0x00, 0x40, 0x10, 0x50]
        if row >= self._numlines:
            row = self._numlines - 1
        if (self._cols == 16 and self._numlines == 4):
            self.command(LCD_SETDDRAMADDR | (col + row_offsetsLarge[row]))
        else:
            self.command(LCD_SETDDRAMADDR | (col + row_offsetsDef[row]))

    def setBacklightPin(self, value, pol = POSITIVE ):
        self._backlightPinMask = ( 1 << value )
        self._polarity = pol
        self.setBacklight(BACKLIGHT_OFF)

    def setBacklight(self, value):
        # Check if backlight is available
        # ----------------------------------------------------
        if self._backlightPinMask != 0x0 :
            # Check for polarity to configure mask accordingly
            # ----------------------------------------------------------
            if ((self._polarity == POSITIVE) and (value > 0)) or ((self._polarity == NEGATIVE ) and ( value == 0 )):
                self._backlightStsMask = self._backlightPinMask & LCD_BACKLIGHT
            else:
                self._backlightStsMask = self._backlightPinMask & LCD_NOBACKLIGHT
            self.i2c.write(self._backlightStsMask)

    def showStr(self, strValue, row = -1, align = ALIGN_LEFT):
        if row >= 0:
            index = 0
            if align == ALIGN_RIGHT:
                index = LCD_COLUMN_NUM - len(strValue)
            elif align == ALIGN_CENTER:
                index = (LCD_COLUMN_NUM - len(strValue)) / 2
            if index < 0:
                index = 0
            self.setCursor(index, row)

        strInList = list(strValue)
        for i in strInList:
            if type(i) == int:
                self.write(i)
            else:
                self.write(ord(i))

    def showCharInInt(self, char):
        self.write(char)

    def send(self, value, mode):
        if mode == FOUR_BITS:
            self.write4bits(value & 0x0F, COMMAND)
        else:
            self.write4bits(value >> 4, mode)
            self.write4bits(value & 0x0F, mode)

    def write(self, value):
        self.send(value, DATA)

    def write4bits(self, value, mode):
        pinMapValue = 0;

        # Map the value to LCD pin mapping
        # --------------------------------
        for i in range(4):
            if (value & 0x1) == 1:
                pinMapValue |= self._data_pins[i]
            value = (value >> 1)

        # Is it a command or data
        # -----------------------
        if mode == DATA:
            mode = self._Rs;

        pinMapValue |= (mode | self._backlightStsMask)
        self.pulseEnable(pinMapValue)

    def pulseEnable(self, data):
        self.i2c.write(data | self._En)
        self.i2c.write(data & ~self._En)

    def command(self, value):
        self.send(value, COMMAND)

    def clean(self):
        # Clean all the screen
        print "Clean...."

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

#lcd = LCD(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin)
lcd = LCD()
if __name__ == "__main__":

    # Multi-chip example
    lcd.begin(LCD_COLUMN_NUM, LCD_ROW_NUM, LCD_2LINE)
    lcd.setBacklightPin(BACKLIGHT_PIN, BACKLIGHT_FLAG)
    lcd.setBacklight(LED_ON)
    lcd.clear()
    #time.sleep(1)
    lcd.home()
    #lcd.setCursor(0, 1)
    #lcd.showStr("Hello world!", 0, ALIGN_CENTER)
    #lcd.showStr("Raspberry Pi", 1, ALIGN_CENTER)
    #lcd.showStr("Roaster controller", 2, ALIGN_CENTER)
    #lcd.showStr("meetate.blogspot.tw", 3, ALIGN_CENTER)
    lcd.setCursor(0, 0)
    for i in range(223, 255):
        lcd.showCharInInt(i);
    '''
    rss.enable()
    time.sleep(1000)
    t1 = Thread(target=pc.getPowerSetting, args=(0.3,))
    t2 = Thread(target=pc.outputWatt, args=())
    t1.start()
    t2.start()
    #t1 = thread.start_new_thread(pc.getPowerSetting, (0.3,))
    #t2 = thread.start_new_thread(pc.outputWatt, ())
    while 1 :
        time.sleep(0.2)
        print "Power:  %.f" % pc.getPower()
    '''
