ALIGN_LEFT        = 0
ALIGN_CENTER    = 1
ALIGN_RIGHT        = 2

class LCDDisplay(object):
    def begin(self, cols, lines, dotsize):
        pass

    def setBacklightPin(self, value, pol):
        pass

    def backlightOn(self):
        pass

    def backlightOff(self):
        pass

    def clear(self):
        pass

    def home(self):
        pass

    def setCursor(self, col, row):
        pass

    def showStr(self, strValue, row, align = ALIGN_LEFT):
        print strValue


class FakeLCDDisplay(LCDDisplay):
    def __init__(self):
        pass
