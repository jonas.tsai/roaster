#!/usr/bin/python
import sys
import time
import math

from max31855.max31855 import MAX31855, MAX31855Error
from htu21d.htu21d import HTU21D

cs_pin = 24
clock_pin = 19 # 23
data_pin = 26 # 22
units = "c"
i=0
maxdiff=0
diff=0
thermocouple = MAX31855(cs_pin, clock_pin, data_pin, units)
while 1:
    i = i + 1
    t1 = thermocouple.get()
    print "-------------------------------------------"
    print "  Count : ", i
    print "  Temp 1: ", t1, "C"
    time.sleep(2)
thermocouple.cleanup()
