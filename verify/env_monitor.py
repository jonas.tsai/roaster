#!/usr/bin/python
import sys
import time
import math, signal, sys

from max31855.max31855 import MAX31855, MAX31855Error
from htu21d.htu21d import HTU21D

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    rc.stop()
    rc.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

cs_pin = 24
clock_pin = 23
data_pin = 26 #22
units = "c"
i=0
maxdiff=0
diff=0
thermocouple = MAX31855(cs_pin, clock_pin, data_pin, units)
#thermocouple = MAX31855()
obj = HTU21D()
while 1:
    i = i + 1
    t1 = thermocouple.read_tmperature()
    t2 = obj.read_tmperature()
    diff = math.fabs(t1-t2)
    if diff > maxdiff:
        maxdiff = diff
    print "-------------------------------------------"
    #print "  Count : ", i
    #print "   diff : ", diff
    #print "Maxdiff : ", maxdiff
    print "  Temp 1: ", t1, "C"
    print "  Temp 2: ", t2, "C"
    print "  Humid : ", obj.read_humidity(), "% rH"
    time.sleep(10)
thermocouple.cleanup()
