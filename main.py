#!/usr/bin/python
import os
from libs.sysinfo import SystemInfo
import sys, time, math, signal
if not SystemInfo.getSimulation():
    import RPi.GPIO as GPIO
    from controller.powerctrl import PowerController
    from controller.fanctrl import FanController
    from sensors.max31855 import MAX31855, MAX31855Error
    from sensors.htu21d import HTU21D
    #from sensors.am2302 import AM2302
    from ads1015.Adafruit_ADS1x15 import ADS1x15
    from keyboard.kb4x4 import KB4x4
else:
    from sensors.fakesensor import *
    from controller.fanctrl import FakeFanController
    from controller.powerctrl import FakePowerController
    from keyboard.gpiokb import ConsoleKB

from libs.roasterdisplay import *
from threading import Thread

from libs.timer import ElapseTimer
from libs.recorder import *
from libs.robot import *
from libs.profile import *
from libs.workingstate import *

m3_cs_pin        = 24
m3_clock_pin     = 23
m3_data_pin      = 26 #22
m3_units         = "c"

pc_en_pin        = 7

fan_pwm_pin      = 29
fan_en_pin       = 31

kb_01_pin        = 11
kb_02_pin        = 12
kb_03_pin        = 13
kb_04_pin        = 15
kb_05_pin        = 16
kb_06_pin        = 18
kb_07_pin        = 19
kb_08_pin        = 21

ads_gain = 4096
ads_sps = 250

class RosaterController(object):
    def __init__(self):
        print "initialing modules... "
        #self.htu = AM2302(25)
        if SystemInfo.getSimulation():
            self.tc = FakeThermocouple()
            self.htu = FakeHTSensor()
            self.pc = FakePowerController(pc_en_pin)
            self.fc = FakeFanController(fan_pwm_pin, fan_en_pin)
            self.kb = ConsoleKB()
        else:
            self.tc = MAX31855(m3_cs_pin, m3_clock_pin, m3_data_pin, m3_units)
            self.htu = HTU21D()
            self.pc = PowerController(pc_en_pin)
            self.fc = FanController(fan_pwm_pin, fan_en_pin)
            self.kb = KB4x4()
        self.timer = [ElapseTimer(), ElapseTimer()]
        self.sysinfo = SystemInfo()
        self.display = RoasterDisplay()
        self.recorder = RoasterRecorder()
        self.robot = RoasterRobot()

        # add subject to observer
        print "Initial observer..."
        self.tc.attach(RoastOperate.Main)
        self.htu.attach(RoastOperate.SysInfo)
        self.pc.attach(RoastOperate.Main)
        self.fc.attach(RoastOperate.Main)
        self.timer[0].attach(RoastOperate.Main)
        self.timer[1].attach(RoastOperate.Main)
        self.timer[0].attach(self.recorder)
        self.timer[0].attach(self.robot)

        # sysinfo will poll roaster status
        print "Setup system info module..."
        self.sysinfo.setRoasterSensor(self.tc)
        self.sysinfo.setEnvSensor(self.htu)
        self.sysinfo.setPowerController(self.pc)
        self.sysinfo.setFanController(self.fc)
        self.sysinfo.setTimer(self.timer)

        # set working state to main
        self.workingState = RoastOperate.Main
        # set components to display
        print "Setup display..."
        self.display.setCurrentState(self.workingState)
        '''
        self.display.setRoasterSensor(self.tc)
        self.display.setEnvSensor(self.htu)
        self.display.setPowerController(self.pc)
        self.display.setFanController(self.fc)
        self.display.setTimer(self.timer)
        self.display.setSystemInfo(self.sysinfo)
        self.display.setRoastRecorder(self.recorder)
        '''

        # set timer and sensors to recorder
        self.recorder.setTimer(self.timer[0])
        self.recorder.setRoasterSensor(self.tc)
        self.recorder.setEnvSensor(self.htu)
        self.recorder.setPowerController(self.pc)
        self.recorder.setFanController(self.fc)
        #self.recorder.startNewRoastRecord()

        # set components to robot for it to control power/fan
        self.robot.setTimer(self.timer)
        self.robot.setRoasterSensor(self.tc)
        self.robot.setPowerController(self.pc)
        self.robot.setFanController(self.fc)

        # Set modules to each state objects for them to control the system
        RoastOperate.Main.setRoasterSensor(self.tc)
        RoastOperate.Main.setPowerController(self.pc)
        RoastOperate.Main.setFanController(self.fc)
        RoastOperate.Main.setTimer(self.timer)
        RoastOperate.Main.setRoastRecorder(self.recorder)
        RoastOperate.Main.setRobot(self.robot)

        RoastOperate.ProfileList.setRobot(self.robot)
        RoastOperate.ProfileDetail.setRobot(self.robot)

        RoastOperate.SysInfo.setSystemInfo(self.sysinfo)
        RoastOperate.SysInfo.setEnvSensor(self.htu)
        RoastOperate.SysInfo.setRobot(self.robot)
        RoastOperate.LogList.setRoastRecorder(self.recorder)

        self.displayIndex = 0
        self.displayMode = 0
        self.running = False


    def start(self):
        print "Start main thread..."
        self.running = True
        self.sysinfo.setRunning(True)
        # set the Power and Fan to 0 %
        self.pc.setPower(0)
        self.fc.setPower(0)
        self.pc.setRunning(True)
        self.fc.setRunning(True)
        # Preparing therad to update and get infomation
        self.tr1 = Thread(target=self.sysinfo.updateSensorInfo, args=(0.1,))
        ##self.tr1 = Thread(target=self.pc.getPowerSetting, args=(0.3,))
        #self.tr2 = Thread(target=self.pc.outputWatt, args=())
        self.tr3 = Thread(target=self.display.updateDisplay, args=(0.1,))
        self.tr4 = Thread(target=self.sysinfo.updateTimerInfo, args=(0.1,))
        self.tr1.start()
        #self.tr2.start()
        self.tr3.start()
        self.tr4.start()
        self.display.showWelcome()
        #time.sleep(3)
        self.display.changeDisplayIndex()

    def stop(self):
        if not self.running:
            return
        self.running = False
        self.pc.setRunning(False)
        self.fc.setRunning(False)
        self.robot.setRunning(False)
        self.display.setRunning(False)
        self.sysinfo.setRunning(False)
        self.tr1.join()
        #self.tr2.join()
        self.tr3.join()
        self.tr4.join()
        self.display.cleanScreen()
        self.display.backlightOff()

    def initPage(self):
        self.display.showWelcome()

    def changePageMode(self):
        self.displayMode += 1
        if self.displayMode == 0:
            self.display.changeDisplayIndex()
            time.sleep(0.2)
            self.display.backlightOn()
        elif self.displayMode == 1:
            self.display.showWelcome()
        else:
            self.display.backlightOff()
            self.displayMode = -1

    def waitingCmd(self):
        # command:
        #    roast start, roast stop, select profile, set power, set fan
        #    timer2 start, timer2 stop, timer2 clear, display change
        # define # as roast start and confirm
        # define * as roast stop and cancel
        # define A as power control and up key
        # define B as fan control or down down
        # define C as timer 2
        # define D as display change
        # roast start:
        #    press #, all the profiles are listed
        #    press # and then #, start roast without loading profile
        #    press #, then number N(2 digital max), and then #, load profile number N
        #    press #, press A and B can navigate the profile, press # to load the profile
        #        if prefile is loaded, the profile content is loaded, A and B can used to navigate
        #    press # after the profile is loaded, start roast according to the profile
        # roast stop: press * and then #
        # set Power: press A, then number N(3Max), and then A again
        #    A and than A as the shortkey to 100%
        # set Fan: press B, then number N(3Max), and then B again
        #    B and then B as the shortkey to 100%
        # Control timer2:
        #    C->#, start
        #    C->*, stop
        #    C->C, reset
        '''
        while 1:
            print "Waiting for keyevent: "
            cmd = self.kb.getKeyInput()
            print "Get key event: " + cmd
            elif cmd == "D":
                print "Change display index"
                #self.changePageMode()
                #self.display.changeDisplayIndex()
                self.displayIndex += 1
                self.display.showDisplayIndex(self.displayIndex)
                if self.displayIndex >= 3:
                    self.displayIndex = -1
        '''
    def cleanup(self):
        self.pc.cleanup()
        self.fc.cleanup()
        self.tc.cleanup()
        if not SystemInfo.getSimulation():
            GPIO.cleanup()
            #self.display.cleanup()

# add signal handler to stop the program
def signal_handler(signal, frame):
    print 'Waiting for un-finished jobs...'
    rc.stop()
    rc.cleanup()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
    os.chdir(os.path.dirname(sys.argv[0]))
    rc = RosaterController()

    rc.start()
    while 1:
        rc.workingState = rc.workingState.perform(rc.kb)
        rc.workingState.setChanged(True)
        rc.display.setCurrentState(rc.workingState)
    #rc.waitingCmd()

